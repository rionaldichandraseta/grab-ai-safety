# Grab AI For Sea Challenge - Safety

## Installing Environment
Install [Miniconda3](http://conda.pydata.org/miniconda.html) if you have not done so. After installing, be sure you have closed and then re-opened the terminal window so the changes can take effect.

Create the `grab-safety` environment from the file `environment.yml` using the following command:
```
conda env create -f environment.yml
```

And then activate the environment using the following command:
```
conda activate grab-safety
```
or
```
source activate grab-safety
```

## Running the Notebook
To run the jupyter notebook, use the following command:
```
jupyter notebook
```
It will open a new tab in the browser and then select the file `grab-safety.ipynb`. Click "Run All" to run all the cells, or go straight to the Testing section in the notebook to test it using the test data.

Change the part that defines the variables for list of data file path and label data file path. Let the notebook preprocess the data for a while before using the saved Gradient Boosting classifier to predict the test data.

The notebook will also give the AUC-ROC score using the `roc_auc_score` from `sklearn.metrics`.
